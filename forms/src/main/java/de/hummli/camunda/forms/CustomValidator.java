package de.hummli.camunda.forms;

import org.camunda.bpm.engine.impl.form.validator.FormFieldValidator;
import org.camunda.bpm.engine.impl.form.validator.FormFieldValidatorContext;

public class CustomValidator implements FormFieldValidator {
	
	@Override
	public boolean validate(Object submittedValue, FormFieldValidatorContext validatorContext) {
		System.out.println(submittedValue.toString());
		return true;	
	}
}
